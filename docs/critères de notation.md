### Notation
Le système de Notation :
```
A B C D E F
```

### critères pour produit de consommation
- écologique
- durable
- respect du vivant
- socialement engagé
- politique
- kilométrage produit
- taille du groupe

| Aspects | Note | Détail |
| -------- | -------- | -------- |
| Respect du vivant     | 🐋🐋🐋🐋🐋     | Text     |
| socialement engagé     | 💚💚💚💚💚     | Text     |
| écologique     | 🍄🍄🍄🍄🍄     | Text     |
| Taille du groupe     | 🐜🐜🐜🐜🐜    | Text     |
| durable     | ⏳⏳⏳⏳⏳     | Text     |
| Positionnement politique     | 📖📖📖📖📖     | Text     |
| kilométrage produit | 🌍🌎🌏🌐🌐 | |

Si un critère est trop mauvais (sauf pour kilométrage et taille du groupe) alors la marque tombe dans "non-éthique"
