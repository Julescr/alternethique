<?php

namespace Database\Factories;

use App\Models\Supporters;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupportersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Supporters::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
