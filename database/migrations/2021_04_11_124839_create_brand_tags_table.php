<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_tags', function (Blueprint $table) {
            $table->integer('brand_id');
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('tag_id');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_tags');
    }
}