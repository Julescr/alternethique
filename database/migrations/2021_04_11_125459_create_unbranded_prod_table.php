<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnbrandedProdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unbranded_prod', function (Blueprint $table) {
            $table->integer('category_id');
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('stores_id');
            $table->foreign('stores_id')
                ->references('id')
                ->on('stores')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unbranded_prod');
    }
}