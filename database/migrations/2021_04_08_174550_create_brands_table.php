<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('descr');
            $table->char('rank');
            $table->integer('parent_id')->nullable();
            $table->timestamps();
            $table->integer('num_searched')->default('0');
            $table->string('category_name')->nullable();
            $table->string('image')->nullable();
        });
        Schema::table('brands', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('brands')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}