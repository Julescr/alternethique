<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_tags', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->integer('tag_id');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('blog_id');
            $table->foreign('blog_id')
                ->references('id')
                ->on('blogs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_tags');
    }
}