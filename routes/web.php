<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogsController;

use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Select2SearchController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

// Route::get('/categories', function () {
//     return view('pages.categories');
// });

Route::get('/blog', function () {
    return view('pages.blog');
});

Route::get('/about', function () {
    return view('pages.about');
});



Route::get('/brands', function () {
    return view('pages.brand');
});
Route::get('/brands/{brand_name}', [BrandsController::class, 'respage'])->name('alternatives');


Route::get('/', [BrandsController::class, 'list_of_brands'])->name('brands_of_moment');

Route::get('/search', [BrandsController::class, 'search'])->name('search');

// Test routes
Route::get('/search2', [Select2SearchController::class, 'index']);
Route::get('/ajax-autocomplete-search', [Select2SearchController::class, 'selectSearch']);
//

Route::get('/home/blog/{blogs}', [BlogsController::class, 'single_blog'])->name('single_blog');

Route::get('/home/blog', [BlogsController::class, 'list_of_blogs'])->name('list_of_blogs');

Route::get('/home/blog/category/{category}', [BlogsController::class, 'category_blogs'])->name('category_blogs');
Route::get('/home/blog/tag/{tag}', [BlogsController::class, 'tag_blogs'])->name('tag_blogs');

Route::get('/home/category', [CategoryController::class, 'all_brands'])->name('all_brands_category');

Route::get('/home/category/{category}', [CategoryController::class, 'category_brands'])->name('category_brands');

// Route::get('/', [CategoryController::class, 'list_of_categories'])->name('list_of_categories');
// Route::get('/', [TagController::class, 'list_of_tags'])->name('list_of_tags');

// Route::get('/', [BrandsController::class, 'people_searches_for'])->name('visitors_searches');
