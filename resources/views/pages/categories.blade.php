@extends('layouts.main')


@section('content')
<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
    <div class="container">
        @if ($category_name)
        <h2>Brands in {{$category_name}}</h2>
        <ol>
            <li><a href="/">Home</a></li>
            <li><a href="{{ url('home/category')}}">Category</a></li>
            <li>{{$category_name}}</li>
            <!-- <li><a href="/">Subcategory</a></li> -->

        </ol>
        @else
        <h2>All Brands</h2>
        <ol>
            <li><a href="/">Home</a></li>
            <li>Category</li>


        </ol>
        @endif

    </div>
</section><!-- End Breadcrumbs -->

@if ($category_name)
<section id="about_category" class="about_category">
    <div class="container" data-aos="fade-up">

        <div class="row">
            <div class="col-lg-4" data-aos="fade-left" data-aos-delay="100">
                <img src="{{ asset('img/categories/'.$category->image)}}" class="img-fluid " alt="">
            </div>
            <div class="col-lg-7 content d-flex flex-column justify-content-center" data-aos="fade-up"
                data-aos-delay="100">
                <h3>{{$category->name}}</h3>

                <!-- <p class="fst-italic">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                      magna aliqua.
                    </p> -->
                @if ($category->descr)
                <p>{{ $category->descr }}</p>

                @else
                <p>Fromagerie végétale bio, produit à La Croix-Saint-Ouen, dans le département de l'Oise (60).
                    Leurs produits sont vendus à la coupe dans la boutique "La crèmerie végétale de Jay&Joy" située dans
                    le 11ème arrondissement à Paris, ainsi que dans certaines épiceries bio. Ils proposent du fromage
                    végétale
                    mais aussi des pâtés végétaux tels que le « joie gras ». </p>
                @endif



            </div>
        </div>

    </div>
</section>
@else

@endif


<section id="blog" class="blog">
    <div class="container" data-aos="fade-up">

        <div class="row">

            <div class="col-lg-8 entries">

                <section id="team2" class="team2">
                    <div class="container" data-aos="fade-up">
                        @if ($category_name)
                        @if ($category->all_brands())
                        @foreach($category->all_brands() as $one_brand)


                        <div class="row">
                            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                <div class="pic"><a href="{{ url('/brands/' . $one_brand->name) }}"><img
                                            src="{{ asset('img/brands/'.$one_brand->image)}}" class="img-fluid"
                                            alt=""></a>
                                </div>
                                <div class="member-info">
                                    <a href="{{ url('/brands/' . $one_brand->name) }}">
                                        <h4>{{$one_brand->name}}</h4>
                                    </a>
                                    <a
                                        href="{{ url('/home/category', $one_brand->categories->first()->id) }}"><span>{{$one_brand->categories->first()->name}}</span></a>

                                    <p>{{$one_brand->descr}}</p>

                                    <!-- <p>

                                                    <button type="button" class="btn btn-outline-primary btn-sm"> en
                                                        vrac </button>

                                                    <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                                    </button>
                                                    <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                                    </button>
                                                </p> -->



                                </div>

                                <div class="member-footer2 fixed-bottom border-top">
                                    @if ($one_brand->tags)
                                    @foreach ($one_brand->tags as $btag)
                                    <button type="button" class="btn btn-outline-primary btn-sm"> {{$btag->name}}
                                    </button>

                                    @endforeach
                                    @else
                                    <button type="button" class="btn btn-outline-primary btn-sm"> All </button>

                                    @endif



                                    <!-- <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                    </button>
                                    <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                    </button> -->



                                </div>

                            </div>
                        </div>




                        @endforeach

                        @else
                        <p>There are no brands in this {{ $sub_category->name }}</p>

                        @endif

                        @else

                        @foreach ($parent_categories as $category5)
                        @if ($category5->all_brands())
                        @foreach($category5->all_brands() as $one_brand)


                        <div class="row">
                            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                <div class="pic"><a href="{{ url('/brands/' . $one_brand->name) }}"><img
                                    src="{{ asset('img/brands/'.$one_brand->image)}}" class="img-fluid"
                                    alt=""></a>
                                </div>
                                <div class="member-info">
                                    <a href="{{ url('/brands/' . $one_brand->name) }}"><h4>{{$one_brand->name}}</h4></a>
                                    <a
                                        href="{{ url('/home/category', $one_brand->categories->first()->id) }}"><span>{{$one_brand->categories->first()->name}}</span></a>

                                    <p>{{$one_brand->descr}}</p>

                                    <!-- <p>

                                                    <button type="button" class="btn btn-outline-primary btn-sm"> en
                                                        vrac </button>

                                                    <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                                    </button>
                                                    <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                                    </button>
                                                </p> -->



                                </div>

                                <div class="member-footer2 fixed-bottom border-top">
                                    @if ($one_brand->tags)
                                    @foreach ($one_brand->tags as $btag)
                                    <button type="button" class="btn btn-outline-primary btn-sm"> {{$btag->name}}
                                    </button>

                                    @endforeach
                                    @else
                                    <button type="button" class="btn btn-outline-primary btn-sm"> All </button>

                                    @endif



                                    <!-- <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                    </button>
                                    <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                    </button> -->



                                </div>

                            </div>
                        </div>




                        @endforeach

                        @else
                        <p>There are no brands in this {{ $sub_category->name }}</p>

                        @endif

                        @endforeach

                        @endif







                </section><!-- End Team Section -->



            </div>

            <div class="col-lg-4">

                <div class="sidebar">


                    <!-- End sidebar search formn-->

                    <h3 class="sidebar-title">Categories</h3>
                    <div class="sidebar-item categories">

                        <ul>


                            @if ($parent_categories->count())
                            @foreach ($parent_categories as $category2)
                                <li>
                                    <a href="{{ url('home/category', $category2) }}">

                                        {{$category2->name}}

                                        <span> {{$category2->count_brands_category()}}</span>

                                    </a>
                                    @if ($category2->children)

                                    @else

                                    @endif

                                    @if ($category2->children->count())
                                        @php($catname = str_replace(' ', '', $category2->name))
                                        <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                            </svg>
                                        </button>

                                        <div class="collapse" id="{{$catname}}">
                                            <ol>
                                                @foreach ($category2->children as $child_cat1)
                                                   <li>
                                                    <a href="{{ url('home/category', $child_cat1) }}">

                                                        {{$child_cat1->name}}

                                                        <span> {{$child_cat1->count_brands_category()}}</span>

                                                    </a>

                                                    {{-- Sub children category --}}
                                                    @if ($child_cat1->children->count())
                                                    @php($catname = str_replace(' ', '', $child_cat1->name))
                                                    <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                        </svg>
                                                    </button>

                                                    <div class="collapse" id="{{$catname}}">
                                                        <ol>
                                                            @foreach ($child_cat1->children as $child_cat2)
                                                               <li>
                                                                <a href="{{ url('home/category', $child_cat2) }}">

                                                                    {{$child_cat2->name}}

                                                                    <span> {{$child_cat2->count_brands_category()}}</span>

                                                                </a>
                                                                    {{-- Sub-sub childs cteagories --}}
                                                                    @if ($child_cat2->children->count())
                                                                    @php($catname = str_replace(' ', '', $child_cat2->name))
                                                                    <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                                        </svg>
                                                                    </button>

                                                                    <div class="collapse" id="{{$catname}}">
                                                                        <ol>
                                                                            @foreach ($child_cat2->children as $child_cat3)
                                                                               <li>
                                                                                <a href="{{ url('home/category', $child_cat3) }}">

                                                                                    {{$child_cat3->name}}

                                                                                    <span> {{$child_cat3->count_brands_category()}}</span>

                                                                                </a>
                                                                                    {{-- Sub-sub childs cteagories --}}


                                                                                    {{--  --}}
                                                                                </li>
                                                                            @endforeach
                                                                            </ol>

                                                                    </div>


                                                                     @endif

                                                                    {{--  --}}
                                                                </li>
                                                            @endforeach
                                                            </ol>

                                                    </div>


                                                     @endif
                                                    {{--  --}}
                                                    </li>
                                                @endforeach
                                                </ol>

                                        </div>


                                    @endif


                                </li>
                            @endforeach
                            @else
                            <p>There is no categories</p>
                            @endif

                        </ul>
                    </div><!-- End sidebar categories-->

                    <h3 class="sidebar-title">Recent Posts</h3>
                    <div class="sidebar-item recent-posts">
                        @if ($category_name)
                        @if ($category->blogs->count())
                        @foreach ($category->blogs as $recent_blog)
                        @if ($loop->iteration == 6)
                        @break
                        @endif
                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/'.$recent_blog->image)}}" alt="">
                            <h4><a href="{{ url('home/blog', $recent_blog) }}">{{$recent_blog->title}}</a></h4>

                            <time datetime="2020-01-01">{{$recent_blog->published_at}}</time>
                        </div>
                        @endforeach
                        @else
                        <p>There is no posts in this category</p>
                        @endif
                        @else

                        @if ($recent_blogs->count())
                        @foreach ($recent_blogs as $recent_blog)
                        @if ($loop->iteration == 6)
                        @break
                        @endif
                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/'.$recent_blog->image)}}" alt="">
                            <h4><a href="{{ url('home/blog', $recent_blog) }}">{{$recent_blog->title}}</a></h4>

                            <time datetime="2020-01-01">{{$recent_blog->published_at}}</time>
                        </div>
                        @endforeach
                        @else
                        <p>There is no posts in this category</p>
                        @endif

                        @endif





                    </div><!-- End sidebar recent posts-->

                    <h3 class="sidebar-title">Tags</h3>
                    <div class="sidebar-item tags">
                        <ul>
                            <li><a href="#">All</a></li>
                            @if ($tags->count())
                            @foreach ($tags as $tag)
                            <li><a href="{{ url('home/blog/tag', $tag) }}">{{$tag->name}}</a></li>
                            @endforeach
                            @else
                            <p>There is no tags</p>
                            @endif
                        </ul>
                    </div>
                    <!-- End sidebar tags-->
                    <!--
                        <div class="widget-sidebar2 widget-tags">
                            <div class="sidebar2-content">
                                <ul>
                                    <li>
                                        <a href="#">All</a>
                                    </li>
                                    <li>
                                        <a href="#">en vrac</a>
                                    </li>
                                    <li>
                                        <a href="#">souple</a>
                                    </li>
                                    <li>
                                        <a href="#">bois</a>
                                    </li>
                                    <li>
                                        <a href="#">vente en ligne</a>
                                    </li>
                                    <li>
                                        <a href="#">produits culturels</a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->

                </div>
                <!-- End sidebar -->

            </div><!-- End blog sidebar -->



            <!-- End blog sidebar -->

        </div>

    </div>
</section><!-- End Blog Section -->
@endsection
