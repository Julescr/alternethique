@extends('layouts.main')

@section('content')



<section id="about" class="about">

    <div class="container" data-aos="fade-up">
    @foreach ($searchResults->groupByType() as $type => $modelSearchResults)


    <div class="row">
        <div class="col">
            <h4>{{ $type }}</h4>
            {{-- <p>{{ $modelSearchResults }}</p> --}}
            @foreach ($modelSearchResults as $searchResult)
                <ul>
                    @if ($searchResult->type == 'brands')
                        <a href="{{ url('/brands/' . $searchResult->title) }} ">{{ $searchResult->title }}</a>
                    @endif

                    @if ($searchResult->type == 'categories')
                        <a href="{{ url('/home/category/' . $searchResult->id) }} ">{{ $searchResult->title }}</a>
                    @endif

                    @if ($searchResult->type == 'tags')
                        <a href="{{ url('/helper/' . $searchResult->id) }} ">{{ $searchResult->title }}</a>
                    @endif

                    @if ($searchResult->type == 'blogs')
                        <a href="{{ url('/home/blog/' . $searchResult->id) }} ">{{ $searchResult->title }}</a>
                    @endif




                </ul>
            @endforeach
        @endforeach

        </div>

    </div>

        </div>
    </section>

@endsection
