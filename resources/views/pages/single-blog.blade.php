@extends('layouts.main')


@section('content')
<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
    <div class="container">
        <!-- <h2>Blog Post</h2> -->

        <ol>
            <li><a href="/">Home</a></li>
            <li><a href="/home/blog">Blog</a></li>
            <li><a href="{{ url('home/blog/category', $blog->category) }}">{{$blog->category->name}}</a></li>
            <li>Blog-id-{{$blog->id}}</li>
        </ol>

    </div>
</section><!-- End Breadcrumbs -->

<!-- ======= Blog Single Section ======= -->
<section id="blog" class="blog">
    <div class="container" data-aos="fade-up">

        <div class="row">

            <div class="col-lg-8 entries">

                <article class="entry entry-single">

                    <div class="entry-img">
                        <img src="{{ asset('img/blog/'.$blog->image)}}" alt="" class="img-fluid">
                    </div>

                    <h2 class="entry-title">
                        <a href="">{{$blog->title}}</a>
                    </h2>

                    <div class="entry-meta">
                        <ul>
                            <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a
                                    href="blog-single.html">{{$blog->author->name}}</a></li>
                            <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="#"><time
                                        datetime="2020-01-01">{{$blog->published_at}}</time></a></li>
                            <!-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li> -->
                        </ul>
                    </div>

                    <div class="entry-content">
                        <?php
                        echo $blog->content;
                        ?>
                        <div class="entry-footer">
                            <i class="bi bi-folder"></i>
                            <ul class="cats">
                                <li><a
                                        href="{{ url('home/blog/category', $blog->category) }}">{{$blog->category->name}}</a>
                                </li>
                            </ul>

                            <i class="bi bi-tags"></i>
                            <ul class="tags">
                                @if ($blog->tags->count())
                                @foreach ($blog->tags as $tag)
                                <li><a href="#">{{$tag->name}}</a></li>
                                @endforeach
                                @else
                                <p>There is no tags for this blog!</p>
                                @endif
                                <!-- <li><a href="#">Creative</a></li>
                                <li><a href="#">Tips</a></li>
                                <li><a href="#">produits frais</a></li> -->
                            </ul>
                        </div>

                        <!-- {{$blog->content}} -->
                    </div>

                </article><!-- End blog entry -->

                <div class="blog-author d-flex align-items-center">
                    <!-- <img src="assets/img/blog/blog-author.jpg" class="rounded-circle float-left" alt=""> -->
                    <div>
                        <h3>Author</h3>
                        <h4>{{$blog->author->name}}</h4>
                        <p>{{$blog->author->email}}</p>
                        <div class="social-links">
                            <a href="https://twitters.com/#"><i class="bi bi-twitter"></i></a>
                            <a href="https://facebook.com/#"><i class="bi bi-facebook"></i></a>
                            <a href="https://instagram.com/#"><i class="biu bi-instagram"></i></a>
                        </div>
                        <!-- <p>
                  Itaque quidem optio quia voluptatibus dolorem dolor. Modi eum sed possimus accusantium. Quas repellat voluptatem officia numquam sint aspernatur voluptas. Esse et accusantium ut unde voluptas.
                </p> -->
                    </div>
                </div><!-- End blog author bio -->



            </div><!-- End blog entries list -->

            <div class="col-lg-4">

                <div class="sidebar">
                    <!-- 
              <h3 class="sidebar-title">Search In Blogs</h3>
              <div class="sidebar-item search-form">
                <form action="">
                  <input type="text">
                  <button type="submit"><i class="bi bi-search"></i></button>
                </form>
              </div> -->
                    <!-- End sidebar search formn-->

                    <h3 class="sidebar-title">Categories</h3>
                    <div class="sidebar-item categories">
                        <ul>
                            @if ($categories->count())
                            @foreach ($categories as $category)
                            <li><a href="{{ url('home/blog/category', $category) }}">{{$category->name}}
                                    <span>{{$category->blogs->count()}}</span></a></li>

                            @endforeach
                            @else
                            <p>There is no categories</p>
                            @endif
                            <!-- <li><a href="#">Alimentation <span>(25)</span></a></li>
                            <li><a href="#">Habillement <span>(12)</span></a></li>
                            <li><a href="#">Cosmétiques <span>(5)</span></a></li>
                            <li><a href="#">Habitat <span>(22)</span></a></li>
                            <li><a href="#">Loisirs <span>(8)</span></a></li>
                            <li><a href="#">Monde pro <span>(14)</span></a></li>
                            <li><a href="#">Zoom sur <span>(14)</span></a></li> -->

                        </ul>
                    </div><!-- End sidebar categories-->

                    <h3 class="sidebar-title">Recent Posts</h3>
                    <div class="sidebar-item recent-posts">
                        @if ($recent_blogs->count())
                        @foreach ($recent_blogs as $recent_blog)
                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/'.$recent_blog->image)}}" alt="">
                            <h4><a href="{{ url('home/blog', $recent_blog) }}">{{$recent_blog->title}}</a></h4>

                            <time datetime="2020-01-01">{{$recent_blog->published_at}}</time>
                        </div>
                        @endforeach
                        @else
                        <p>There is no categories</p>
                        @endif
                        <!-- <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/blog1.webp')}}" alt="">
                            <h4><a href="/blog-single">StopBlues, l’application en réponse au mal-être et à la
                                    dépression</a></h4>
                            <time datetime="2020-01-01">Jan 1, 2020</time>
                        </div>

                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/blog2.webp')}}" alt="">
                            <h4><a href="/blog-single">Jours à venir rend accessible l’art de vivre écoresponsable</a>
                            </h4>
                            <time datetime="2020-01-01">Jan 1, 2020</time>
                        </div>

                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/blog3.webp')}}" alt="">
                            <h4><a href="/blog-single">Sexploration : l’éducation à la sexualité avec des jeux de
                                    cartes</a></h4>
                            <time datetime="2020-01-01">Jan 1, 2020</time>
                        </div>

                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/blog4.webp')}}" alt="">
                            <h4><a href="/blog-single">Cultures et Compagnie installe des fermes au sein des entreprises
                                    !</a></h4>
                            <time datetime="2020-01-01">Jan 1, 2020</time>
                        </div>

                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/blog5.webp')}}" alt="">
                            <h4><a href="/blog-single">Une poupée de chiffon française, naturelle et artisanale : Poupée
                                    Odette !</a></h4>
                            <time datetime="2020-01-01">Jan 1, 2020</time>
                        </div> -->

                    </div><!-- End sidebar recent posts-->

                    <h3 class="sidebar-title">Tags</h3>
                    <div class="sidebar-item tags">
                        <ul>
                            <li><a href="{{ url('home/blog') }}">All</a></li>
                            @if ($tags->count())
                            @foreach ($tags as $tag)
                            <li><a href="{{ url('home/blog/tag', $tag) }}">{{$tag->name}}</a></li>
                            @endforeach
                            @else
                            <p>There is no tags</p>
                            @endif


                            <!-- <li><a href="#">souple</a></li>
                            <li><a href="#">bois</a></li>
                            <li><a href="#">vente en ligne</a></li>
                            <li><a href="#">produits culturels</a></li>
                            <li><a href="#">produits hi-tech</a></li>
                            <li><a href="#">produits frais</a></li> -->
                            <!-- <li><a href="#">Smart</a></li>
                  <li><a href="#">Tips</a></li>
                  <li><a href="#">Marketing</a></li> -->
                        </ul>
                    </div><!-- End sidebar tags-->

                </div><!-- End sidebar -->

            </div><!-- End blog sidebar -->

        </div>

    </div>
</section><!-- End Blog Single Section -->



@endsection