@extends('layouts.main')


@section('content')
<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
    <div class="container">
        @if($category_name)
        <h2>Blog Posts about {{$category_name}}</h2>
        @elseif ($tag_name)
        <h2>Blog Posts with tag: {{$tag_name}}</h2>
        @else
        <h2>Blog posts</h2>
        @endif


        <ol>
            <li><a href="/">Home</a></li>
            @if($category_name)
            <li><a href="{{ url('home/blog') }}">Blog</a></li>
            <li>{{$category_name}}</li>
            @else
            <li>Blog</li>
            @endif

        </ol>


    </div>
</section><!-- End Breadcrumbs -->

<section id="blog" class="blog">
    <div class="container" data-aos="fade-up">

        <div class="row">

            <div class="col-lg-8 entries">
                @if ($all_blogs->count())
                @foreach ($all_blogs as $blog)
                <article class="entry">

                    <div class="entry-img"><a href="{{ url('home/blog', $blog) }}">
                            <img src="{{ asset('img/blog/'.$blog->image)}}" alt="" class="img-fluid">
                    </div>

                    <h2 class="entry-title">
                        <a href="{{ url('home/blog', $blog) }}">{{$blog->title}}</a>
                    </h2>

                    <div class="entry-meta">
                        <ul>
                            <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a
                                    href="blog-single.html">{{$blog->author->name}}</a></li>
                            <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a
                                    href="blog-single.html"><time
                                        datetime="2020-01-01">{{$blog->published_at}}</time></a></li>
                            <!-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="blog-single.html">12 Comments</a></li> -->
                        </ul>
                    </div>

                    <div class="entry-content">
                        <p>{{$blog->summary}} </p>
                        <div class="read-more">
                            <a href="{{ url('home/blog', $blog) }}">Read More</a>
                        </div>
                    </div>

                </article><!-- End blog entry -->

                @endforeach
                @else
                <p>There is no blogs!</p>
                @endif



                @if ($all_blogs->count() > 3)
                <div class="blog-pagination">
                    <ul class="justify-content-center">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                    </ul>
                </div>

                @endif


            </div><!-- End blog entries list -->

            <div class="col-lg-4">

                <div class="sidebar">


                    <!-- End sidebar search formn-->

                    <h3 class="sidebar-title">Categories</h3>
                    <div class="sidebar-item categories">

                            {{-- Categories tree --}}
                            <ul>


                                @if ($categories->count())
                                @foreach ($categories as $category2)
                                    <li>
                                        <a href="{{ url('home/blog/category', $category2) }}">

                                            {{$category2->name}}

                                            <span> {{$category2->blogs->count()}}</span>

                                        </a>
                                        @if ($category2->children)

                                        @else

                                        @endif

                                        @if ($category2->children->count())
                                            @php($catname = str_replace(' ', '', $category2->name))
                                            <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                </svg>
                                            </button>

                                            <div class="collapse" id="{{$catname}}">
                                                <ol>
                                                    @foreach ($category2->children as $child_cat1)
                                                       <li>
                                                        <a href="{{ url('home/blog/category', $child_cat1) }}">

                                                            {{$child_cat1->name}}

                                                            <span> {{$child_cat1->blogs->count()}}</span>

                                                        </a>

                                                        {{-- Sub children category --}}
                                                        @if ($child_cat1->children->count())
                                                        @php($catname = str_replace(' ', '', $child_cat1->name))
                                                        <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                                <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                            </svg>
                                                        </button>

                                                        <div class="collapse" id="{{$catname}}">
                                                            <ol>
                                                                @foreach ($child_cat1->children as $child_cat2)
                                                                   <li>
                                                                    <a href="{{ url('home/blog/category', $child_cat2) }}">

                                                                        {{$child_cat2->name}}

                                                                        <span> {{$child_cat2->blogs->count()}}</span>

                                                                    </a>
                                                                        {{-- Sub-sub childs cteagories --}}
                                                                        @if ($child_cat2->children->count())
                                                                        @php($catname = str_replace(' ', '', $child_cat2->name))
                                                                        <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $catname }}" aria-expanded="false" aria-controls="{{$catname}}">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                                                                                <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                                                                            </svg>
                                                                        </button>

                                                                        <div class="collapse" id="{{$catname}}">
                                                                            <ol>
                                                                                @foreach ($child_cat2->children as $child_cat3)
                                                                                   <li>
                                                                                    <a href="{{ url('home/blog/category', $child_cat3) }}">

                                                                                        {{$child_cat3->name}}

                                                                                        <span> {{$child_cat3->blogs->count()}}</span>

                                                                                    </a>
                                                                                        {{-- Sub-sub childs cteagories --}}


                                                                                        {{--  --}}
                                                                                    </li>
                                                                                @endforeach
                                                                                </ol>

                                                                        </div>


                                                                         @endif

                                                                        {{--  --}}
                                                                    </li>
                                                                @endforeach
                                                                </ol>

                                                        </div>


                                                         @endif
                                                        {{--  --}}
                                                        </li>
                                                    @endforeach
                                                    </ol>

                                            </div>


                                        @endif


                                    </li>
                                @endforeach
                                @else
                                <p>There is no categories</p>
                                @endif

                            </ul>
                            {{-- end of tree --}}
                        {{-- <ul>
                            @if ($categories->count())
                            @foreach ($categories as $category)
                            <li><a href="{{ url('home/blog/category', $category) }}">{{$category->name}}
                                    <span>{{$category->blogs->count()}}</span></a></li>

                            @endforeach
                            @else
                            <p>There is no categories</p>
                            @endif
                        </ul> --}}
                    </div><!-- End sidebar categories-->

                    <h3 class="sidebar-title">Recent Posts</h3>
                    <div class="sidebar-item recent-posts">
                        @if ($recent_blogs->count())
                        @foreach ($recent_blogs as $recent_blog)
                        <div class="post-item clearfix">
                            <img src="{{ asset('img/blog/'.$recent_blog->image)}}" alt="">
                            <h4><a href="{{ url('home/blog', $recent_blog) }}">{{$recent_blog->title}}</a></h4>

                            <time datetime="2020-01-01">{{$recent_blog->published_at}}</time>
                        </div>
                        @endforeach
                        @else
                        <p>There is no categories</p>
                        @endif


                    </div><!-- End sidebar recent posts-->

                    <h3 class="sidebar-title">Tags</h3>
                    <div class="sidebar-item tags">
                        <ul>
                            <li><a href="{{ url('home/blog') }}">All</a></li>
                            @if ($tags->count())
                            @foreach ($tags as $tag)
                            <li><a href="{{ url('home/blog/tag', $tag) }}">{{$tag->name}}</a></li>
                            @endforeach
                            @else
                            <p>There is no tags</p>
                            @endif
                        </ul>
                    </div>
                    <!-- End sidebar tags-->
                    <!--
                      <div class="widget-sidebar2 widget-tags">
                        <div class="sidebar2-content">
                            <ul>
                                <li>
                                    <a href="#">All</a>
                                </li>
                                <li>
                                    <a href="#">en vrac</a>
                                </li>
                                <li>
                                    <a href="#">souple</a>
                                </li>
                                <li>
                                    <a href="#">bois</a>
                                </li>
                                <li>
                                    <a href="#">vente en ligne</a>
                                </li>
                                <li>
                                    <a href="#">produits culturels</a>
                                </li>
                            </ul>
                        </div>
                      </div> -->

                </div>
                <!-- End sidebar -->

            </div><!-- End blog sidebar -->

        </div>

    </div>
</section><!-- End Blog Section -->

@endsection
