@extends('layouts.main')


@section('content')
<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
    <div class="container">

        @if ($brands->first()->rank == 'F')
        @if ($brands->first()->image)
        <div class="logo-img">
            <img src="{{ asset('img/brands/' . $brands->first()->image) }}" alt="" class="img-fluid">
        </div>
        @else
        <h1>Alternatives To {{ $brands->first()->name }}</h1>
        @endif


        @endif






        <ol>
            <li><a href="/">Home</a></li>
            @foreach ($brands->first()->categories as $cat3)
            @if ($cat3->all_parents())
            @foreach ($cat3->all_parents() as $par_cat)
            <li><a href="{{ url('/home/category', $par_cat->id) }}">{{ $par_cat->name }}</a></li>
            @endforeach
            {{-- <li><a href="/">{{ $cat3->top_root()->name }}</a></li>

            @foreach ($cat3->top_root()->allChildren as $child_cat)
            @if ($child_cat == $cat3)
            @break
            @else
            <li><a href="/">{{ $child_cat->name }}</a></li>

            @endif
            @endforeach --}}
            @else
            <li><a href="{{ url('/home/category', $par_cat->id) }}">{{ $cat3->name }}</a></li>
            @endif
            @endforeach

            {{-- <li><a href="/">Subcategory</a></li> --}}

        </ol>
    </div>
</section><!-- End Breadcrumbs -->

<section id="about" class="about">
    <div class="container" data-aos="fade-up">

        @if ($brands->first()->rank == 'F')
        @if ($brands->first()->categories->first())
        <div class="row">
            <div class="col-lg-4" data-aos="fade-left" data-aos-delay="100">
                <img src="{{ asset('img/categories/' . $brands->first()->categories->first()->image) }}"
                    class="img-fluid" alt="">
            </div>
            <div class="col-lg-7 content d-flex flex-column justify-content-center" data-aos="fade-up"
                data-aos-delay="100">
                <h3>About category {{ $brands->first()->categories->first()->name }}</h3>
                <!-- <p class="fst-italic">
                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                              magna aliqua.
                                            </p> -->
                @if ($brands->first()->categories->first()->descr)
                <p>{{ $brands->first()->categories->first()->descr }} </p>
                @else
                <p>Fromagerie végétale bio, produit à La Croix-Saint-Ouen, dans le département de l'Oise (60).
                    Leurs produits sont vendus à la coupe dans la boutique "La crèmerie végétale de Jay&Joy"
                    située dans
                    le 11ème arrondissement à Paris, ainsi que dans certaines épiceries bio. Ils proposent du
                    fromage
                    végétale
                    mais aussi des pâtés végétaux tels que le « joie gras ». </p>

                @endif

                <ul>
                    <li>
                        <i class="bx bx-store-alt"></i>
                        <div>
                            <h5>Ullamco laboris nisi ut aliquip consequat</h5>
                            <p>Magni facilis facilis repellendus cum excepturi quaerat praesentium libre trade</p>
                        </div>
                    </li>
                    <li>
                        <i class="bx bx-images"></i>
                        <div>
                            <h5>Magnam soluta odio exercitationem reprehenderi</h5>
                            <p>Quo totam dolorum at pariatur aut distinctio dolorum laudantium illo direna pasata
                                redi
                            </p>
                        </div>
                    </li>
                </ul>

                @if ($brands->first()->rank != 'F')
                <h3>Links </h3>
                <div class="col-lg-6">
                    <a href="#" class="btn btn-primary disabled" tabindex="-1" role="button" aria-disabled="true"><svg
                            xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-globe" viewBox="0 0 16 16">
                            <path
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z" />
                        </svg>
                        Search in Web </a>
                </div>

                @endif



            </div>
        </div>
        @else

        @endif
        {{-- If brand is ethical show about brand --}}
        @else

        @if ($brands->first()->categories->first())
        <div class="row">
            <div class="col-lg-4" data-aos="fade-left" data-aos-delay="100">
                <img src="{{ asset('img/brands/' . $brands->first()->image) }}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-7 content d-flex flex-column justify-content-center" data-aos="fade-up"
                data-aos-delay="100">
                <h3>About {{ $brands->first()->name }}</h3>
                <!-- <p class="fst-italic">
                                              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                              magna aliqua.
                                            </p> -->
                @if ($brands->first()->descr)
                <p>{{ $brands->first()->descr }} </p>
                @else
                <p>Fromagerie végétale bio, produit à La Croix-Saint-Ouen, dans le département de l'Oise (60).
                    Leurs produits sont vendus à la coupe dans la boutique "La crèmerie végétale de Jay&Joy"
                    située dans
                    le 11ème arrondissement à Paris, ainsi que dans certaines épiceries bio. Ils proposent du
                    fromage
                    végétale
                    mais aussi des pâtés végétaux tels que le « joie gras ». </p>

                @endif

                <ul>
                    <li>
                        <i class="bx bx-store-alt"></i>
                        <div>
                            <h5>Ullamco laboris nisi ut aliquip consequat</h5>
                            <p>Magni facilis facilis repellendus cum excepturi quaerat praesentium libre trade</p>
                        </div>
                    </li>
                    <li>
                        <i class="bx bx-images"></i>
                        <div>
                            <h5>Magnam soluta odio exercitationem reprehenderi</h5>
                            <p>Quo totam dolorum at pariatur aut distinctio dolorum laudantium illo direna pasata
                                redi
                            </p>
                        </div>
                    </li>
                </ul>

                <h3>Links </h3>

                <div class="col-lg-6">
                    <a href="#" class="btn btn-primary disabled" tabindex="-1" role="button" aria-disabled="true"><svg
                            xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-globe" viewBox="0 0 16 16">
                            <path
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z" />
                        </svg>
                        Search in Web </a>
                </div>

            </div>
        </div>
        @else

        @endif


        @endif


    </div>

</section>

<section id="blog" class="blog">
    <div class="container" data-aos="fade-up">

        <div class="row">

            <div class="col-lg-12 entries">

                <div class="features">



                    <!-- Feature Tabs -->
                    <!-- Tabs -->
                    <ul class="nav nav-pills mb-3">
                        <li>
                            <a class="nav-link" data-bs-toggle="pill" href="#tab1">Page du vendeur</a>
                        </li>
                        <li>
                            <a class="nav-link active" data-bs-toggle="pill" href="#tab2">Autre marques</a>
                        </li>

                        @if ($brands->first()->rank != 'F' and $brands->first()->stores->count() )
                        <li>
                            <a class="nav-link" data-bs-toggle="pill" href="#tab3">A voir</a>
                        </li>

                        @endif

                    </ul><!-- End Tabs -->

                    <!-- Tab Content -->
                    <div class="tab-content">

                        <div class="tab-pane fade show" id="tab1">
                            <section id="why-us" class="why-us">
                                <div class="container-fluid" data-aos="fade-up">

                                    <div class="row">

                                        <div
                                            class="col d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                                            <div class="content">
                                                @if ($brands->first()->rank == 'F')
                                                <h3>Why <mark>{{ $brands->first()->name }}</mark> Is Bad</h3>
                                                @else
                                                <h3>Why <mark>{{ $brands->first()->name }}</mark> Is Good</h3>

                                                @endif
                                                <h3>Eum ipsam laborum deleniti <strong>velit pariatur architecto aut
                                                        nihil</strong></h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis
                                                    aute irure dolor in reprehenderit
                                                </p>
                                            </div>

                                            <div class="accordion-list">
                                                <ul>
                                                    <li>
                                                        <a data-bs-toggle="collapse" class="collapse"
                                                            data-bs-target="#accordion-list-1"><span>01</span> Non
                                                            consectetur a erat nam at lectus urna duis? <i
                                                                class="bx bx-chevron-down icon-show"></i><i
                                                                class="bx bx-chevron-up icon-close"></i></a>
                                                        <div id="accordion-list-1" class="collapse show"
                                                            data-bs-parent=".accordion-list">
                                                            <p>
                                                                Feugiat pretium nibh ipsum consequat. Tempus iaculis
                                                                urna id volutpat lacus laoreet non curabitur gravida.
                                                                Venenatis lectus magna fringilla urna porttitor rhoncus
                                                                dolor purus non.
                                                            </p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2"
                                                            class="collapsed"><span>02</span> Feugiat scelerisque varius
                                                            morbi enim nunc? <i
                                                                class="bx bx-chevron-down icon-show"></i><i
                                                                class="bx bx-chevron-up icon-close"></i></a>
                                                        <div id="accordion-list-2" class="collapse"
                                                            data-bs-parent=".accordion-list">
                                                            <p>
                                                                Dolor sit amet consectetur adipiscing elit pellentesque
                                                                habitant morbi. Id interdum velit laoreet id donec
                                                                ultrices. Fringilla phasellus faucibus scelerisque
                                                                eleifend donec pretium. Est pellentesque elit
                                                                ullamcorper dignissim. Mauris ultrices eros in cursus
                                                                turpis massa tincidunt dui.
                                                            </p>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3"
                                                            class="collapsed"><span>03</span> Dolor sit amet consectetur
                                                            adipiscing elit? <i
                                                                class="bx bx-chevron-down icon-show"></i><i
                                                                class="bx bx-chevron-up icon-close"></i></a>
                                                        <div id="accordion-list-3" class="collapse"
                                                            data-bs-parent=".accordion-list">
                                                            <p>
                                                                Eleifend mi in nulla posuere sollicitudin aliquam
                                                                ultrices sagittis orci. Faucibus pulvinar elementum
                                                                integer enim. Sem nulla pharetra diam sit amet nisl
                                                                suscipit. Rutrum tellus pellentesque eu tincidunt.
                                                                Lectus urna duis convallis convallis tellus. Urna
                                                                molestie at elementum eu facilisis sed odio morbi quis
                                                            </p>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>

                                        <!-- <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img"
                                                                                style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in"
                                                                                data-aos-delay="150">&nbsp;</div> -->
                                    </div>

                                </div>
                            </section><!-- End Why Us Section -->

                        </div>
                        <!-- End Tab 1 Content -->

                        <div class="tab-pane fade show active" id="tab2">
                            {{-- <div class="col-lg-8 entries"> --}}
                            <section id="team2" class="team2">
                                <div class="container" data-aos="fade-up">


                                    @if ($brands->first()->alternatives())

                                    @foreach ($brands->first()->alternatives() as $brand)

                                    <div class="row">
                                        <div class="member d-flex align-items-start" data-aos="zoom-in"
                                            data-aos-delay="100">
                                            <div class="pic"><a href="{{ url('/brands/' . $brand->name) }}"><img
                                                        src="{{ asset('img/brands/' . $brand->image) }}"
                                                        class="img-fluid" alt=""></a>
                                            </div>
                                            <div class="member-info">
                                                <a href="{{ url('/brands/' . $brand->name) }}">
                                                    <h4>{{ $brand->name }}</h4>
                                                </a>
                                                @foreach ($brand->categories as $cat1)
                                                <a href="{{ url('/home/category', $cat1) }}"><span>{{ $cat1->name }}</span>
                                                </a>
                                                @endforeach



                                                </a>
                                                <p>{{ $brand->descr }}</p>

                                                <!-- <p>

                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> en
                                                                                            vrac </button>

                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                                                                        </button>
                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                                                                        </button>
                                                                                    </p> -->



                                            </div>

                                            <div class="member-footer2 fixed-bottom border-top">
                                                @if ($brand->tags->count())
                                                @foreach ($brand->tags as $tag)
                                                <!-- @if ($loop->iteration > 3) <button type="button" class="btn btn-outline-primary btn-sm"> More.. </button> @break -->

                                                <!-- @else -->
                                                <button type="button" class="btn btn-outline-primary btn-sm">
                                                    {{ $tag->name }} </button>

                                                <!-- @endif -->

                                                @endforeach
                                                @else
                                                <button type="button" class="btn btn-outline-primary btn-sm"> All
                                                </button>

                                                @endif

                                                {{-- <button type="button" class="btn btn-outline-primary btn-sm"> en
                                                    vrac </button>

                                                <button type="button" class="btn btn-outline-primary btn-sm"> souple
                                                </button>
                                                <button type="button" class="btn btn-outline-primary btn-sm"> bois
                                                </button> --}}



                                            </div>

                                        </div>
                                    </div>
                                    @endforeach

                                    @endif

                                </div>

                {{-- </div> --}}
</section><!-- End Team Section -->


</div><!-- End Tab 2 Content -->

<div class="tab-pane fade show" id="tab3">
    @if ($brands->first()->stores)
    @foreach ( $brands->first()->stores as $store1)
    <section id="team" class="team">
        <div class="container" data-aos="fade-up">
            <div class="row">
                <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="400">
                    {{-- {{ asset('img/stores/'.$store1->image) }} --}}
                    <div class="store-pic"><img src="{{ asset('img/stores/market.png')}}" class="img-fluid" alt="">
                    </div>
                    <div class="member-info2">
                        <h4>{{ $store1->name }}</h4>
                        {{-- <a href="#">
                                                    <span>Subcategory</span>


                                                </a> --}}
                        <p> Location: {{ $store1->location }} </p>

                        {{-- <h5> Link </h5> --}}
                        <div class="member-footer2 pt-2">

                            <a href="#" class="btn btn-success" tabindex="-1" role="button" aria-disabled="true"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-house-door" viewBox="0 0 16 16">
                                    <path
                                        d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
                                </svg> Link To Website </a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @endforeach
    @else
    <p> No information about stores for {{ $brands->first()->name }}</p>
    @endif
</div><!-- End Tab 3 Content -->

</div>
</div>


</div>



<!-- End blog sidebar -->

</div>

</div>
</section><!-- End Blog Section -->
@endsection
