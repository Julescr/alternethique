@extends('layouts.app')



@section('hero')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

    <div class="container text-center position-relative">
        <h1 class="logo"><img src="{{ asset('img/logo2.png') }}" alt="" class="img-fluid"><a href="/">
                Alternéthique.fr</a></h1>


        <div class="row">
            <div class="s129">
                <form type="get" action="{{ url('/search') }}">
                    <div class="inner-form">
                        <div class="input-field">

                            <button class="btn-search" type="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path
                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                    </path>
                                </svg>
                            </button>
                            <p class="exp1"><span class="typed"
                                    data-typed-items="Coca-Cola, Nike, Apple, Fanta, McDonalds"></span> </p>

                            <input id="search" name="query" type="search" />
                            <!-- <h2 class="input.placeholder"><span class="typed" data-typed-items="Designer, Developer, Freelancer, Photographer"></span></p> -->

                        </div>
                    </div>
                </form>
            </div>


        </div>

    </div>



</section>
<!-- End Hero -->

@endsection

@section('bottom-header')
<div class="custom-container">
    <!-- <div class="container2"> -->

    <nav class="navbar navbar-dark d-flex bg-transparent">
        <div class="container-fluid">
            <ul class="maincategories">

                @if ($parent_category->count())
                @foreach ($parent_category as $pcat)
                @if ($loop->iteration == 1)
                <li><a margin class="nav-link scrollto" href="{{ url('/home/category', $pcat) }}">{{ $pcat->name }}</a>
                </li>

                @else
                <li><a class="nav-link scrollto" href="{{ url('home/category', $pcat) }}">
                        {{ $pcat->name }}</a></li>

                @endif

                @endforeach
                @else
                <p>There is no parent categories</p>
                @endif
                <!-- <li><a margin class="nav-link scrollto" href="/categories">Alimentation</a></li>
                                                                                                                                                        <li>
                                                                                                                                                        <a-bottom class="nav-link" href="/categories">Habillement</a-bottom>
                                                                                                                                                        </li>
                                                                                                                                                        <li>
                                                                                                                                                        <a-bottom class="nav-link" href="/categories">Cosmétiques</a-bottom>
                                                                                                                                                        </li> -->
            </ul>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>

    <div class="collapse" id="navbarToggleExternalContent">

        <div class="mega-menu mega-menu-categories panels open">
            <div class="container panel">
                <div class="row main-panel panel-body align-content-sm-start ">
                    @if ($parent_category->count())
                    @foreach ($parent_category as $pcat)
                    <div class="col-sm-4 ">
                        <!-- <h4>{{ $pcat->name }}</h4> -->
                        <ul class="sub-categories-menu ">
                            @if ($pcat->children->count())
                            @foreach ($pcat->children as $pcat_child)
                            <li><a href="{{ url('home/category', $pcat_child) }}">{{ $pcat_child->name }}</a>
                            </li>
                            @if ($pcat_child->children->count())
                            <ul class="sub-categories-menu ">
                                @foreach ($pcat_child->children as $pcat_child2)
                                <li><a class="little"
                                        href="{{ url('home/category', $pcat_child2) }}">{{ $pcat_child2->name }}</a>
                                </li>
                                @if ($pcat_child2->children->count())
                                <ul class="sub-categories-menu ">
                                    @foreach ($pcat_child2->children as $pcat_child3)
                                    <li><a class="little"
                                            href="{{ url('home/category', $pcat_child3) }}">{{ $pcat_child3->name }}</a>
                                    </li>

                                    @endforeach
                                </ul>
                                @endif
                                @endforeach
                            </ul>
                            @endif

                            @endforeach
                            @endif
                            <!-- <li><a href="#">Alimentation</a></li> -->
                            {{-- <!-- <li><a href="#">Habillement</a></li>
<li><a href="#">Cosmétiques</a></li>
<ul class="sub-categories-menu ">
<li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
<li><a class="little" href="#">Soins et hygiène</a></li>

</li>

</ul>
<li><a href="#">Habitat</a></li>
<li><a href="#">Loisirs</a></li>
<li><a href="#">Monde pro</a></li>

<ul class="sub-categories-menu ">
<li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
<li><a class="little" href="#">Soins et hygiène</a></li>


</li>

</ul> --> --}}
                        </ul>
                    </div>
                    @endforeach
                    @else
                    <p>There is no parent categories</p>
                    @endif


                    <!-- <div class="col-sm ">
                                                                                                                                                        <h4>Category 2</h4>

                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a href="#">Alimentation</a></li>
                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
                                                                                                                                                        <li><a class="little" href="#">Soins et hygiène</a></li>
                                                                                                                                                        <li><a href="#">Habillement</a></li>
                                                                                                                                                        <li><a href="#">Cosmétiques</a></li>

                                                                                                                                                        </li>

                                                                                                                                                        </ul>
                                                                                                                                                        <li><a href="#">Habitat</a></li>
                                                                                                                                                        <li><a href="#">Loisirs</a></li>
                                                                                                                                                        <li><a href="#">Monde pro</a></li>

                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
                                                                                                                                                        <li><a class="little" href="#">Soins et hygiène</a></li>


                                                                                                                                                        </li>

                                                                                                                                                        </ul>
                                                                                                                                                        </ul>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-sm ">
                                                                                                                                                        <h4>Category 3</h4>

                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a href="#">Alimentation</a></li>
                                                                                                                                                        <li><a href="#">Habillement</a></li>
                                                                                                                                                        <li><a href="#">Cosmétiques</a></li>
                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
                                                                                                                                                        <li><a class="little" href="#">Soins et hygiène</a></li>

                                                                                                                                                        </li>

                                                                                                                                                        </ul>
                                                                                                                                                        <li><a href="#">Habitat</a></li>
                                                                                                                                                        <li><a href="#">Loisirs</a></li>
                                                                                                                                                        <li><a href="#">Monde pro</a></li>

                                                                                                                                                        <ul class="sub-categories-menu ">
                                                                                                                                                        <li><a class="little" href="#">Boutiques en ligne de cosmétiques</a></li>
                                                                                                                                                        <li><a class="little" href="#">Soins et hygiène</a></li>


                                                                                                                                                        </li>

                                                                                                                                                        </ul>
                                                                                                                                                        </ul>
                                                                                                                                                        </div> -->


                </div>
            </div>
        </div>
    </div>


    <!-- </div> -->

</div>
@endsection


@section('search-for-brands')
<!-- ======= Services Section ======= -->
<section id="services" class="services section-bg">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Our Visitors searches for an alternative to</h2>
            <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div class="row">

            @if ($uneth_brands->count())
            @foreach ($uneth_brands as $brand2)
            @if ($brand2->categories->count())
            @foreach ($brand2->categories as $cat5)

            @foreach ($cat5->all_parents() as $cat6)
            @if ($loop->index == 0)

            @if ($cat6->name == 'Alimentation')

            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-18" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>
            @elseif ($cat6->name == 'Internet')


            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-14 color1" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>

            @elseif ($cat6->name == 'Loisir')

            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-13" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>

            @elseif ($cat6->name == 'Service')

            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-19" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>

            @elseif ($cat6->name == 'Habillement')

            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-20 color3" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>

            @elseif ($cat6->name == 'Cosmétique et santé')
            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-20 color4" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>


            @elseif ($cat6->name == 'Habitat')

            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-14 color2" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>
            @elseif ($cat6->name == 'Matériel électronique')
            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-20 color2" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>


            @endif
            @endif

            @endforeach

            @endforeach

            @else
            <a href="{{ url('/brands/' . $brand2->name) }}" class="custom-btn btn-20 color2" role="button"><svg
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                    class="bi bi-tree-fill" viewBox="0 0 24 24">
                    <path
                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                    </path>
                </svg></i>{{ $brand2->name }}</a>



            @endif




            @endforeach
            @else
            <p>There are no brands</p>
            @endif
            <!-- <button class="custom-btn btn-14 color1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Coca-Cola</button>
                                                                                                                                                        <button class="custom-btn btn-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Fanta</button>
                                                                                                                                                        <button class="custom-btn btn-18 color2"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Calvin Klein</button>
                                                                                                                                                        <button class="custom-btn btn-19"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Apple</button>
                                                                                                                                                        <button class="custom-btn btn-20 color3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Magnum</button>
                                                                                                                                                        <button class="custom-btn btn-20 color4"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Coca-Cola</button>
                                                                                                                                                        <button class="custom-btn btn-19"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Fanta</button>
                                                                                                                                                        <button class="custom-btn btn-14"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Calvin Klein</button>
                                                                                                                                                        <button class="custom-btn btn-18"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Apple</button>
                                                                                                                                                        <button class="custom-btn btn-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                                                                                                        fill="currentColor" class="bi bi-tree-fill" viewBox="0 0 24 24">
                                                                                                                                                        <path
                                                                                                                                                        d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z">
                                                                                                                                                        </path>
                                                                                                                                                        </svg></i>Magnum</button> -->

        </div>




    </div>
</section><!-- End Services Section -->

@endsection


@section('brands-moment')
<section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Brands of the moment </h2>
            <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div class="row row-cols-3">

            @if ($top_brands->count())
            @foreach ($top_brands as $brand)
            @if ($brand->rank == 'A')
            <div class="col ">
                <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                    <div class="pic"><a href="{{ url('/brands/' . $brand->name) }}"><img
                                src="{{ asset('img/brands/' . $brand->image) }}" class="img-fluid" alt=""></a>
                    </div>
                    <div class="member-info">
                        <a href="{{ url('/brands/'. $brand->name) }}">
                            <h4>{{  $brand->name }}</h4>
                        </a>
                        @foreach ($brand->categories as $cat1)
                        <a href="{{ url('/home/category', $cat1) }}"><span>{{ $cat1->name }}</span>
                        </a>
                        @endforeach
                        <p>{{ $brand->descr }}</p>
                        <div class="member-footer fixed-bottom border-top">


                            @if ($brand->tags->count())
                            @foreach ($brand->tags as $tag)
                            @if ($loop->iteration > 3)
                            <button type="button" class="btn btn-outline-primary btn-sm">
                                More.. </button>
                            @break
                            @else
                            <button type="button" class="btn btn-outline-primary btn-sm">
                                {{ $tag->name }} </button>

                            @endif

                            @endforeach
                            @else
                            <button type="button" class="btn btn-outline-primary btn-sm"> All </button>

                            @endif





                            <!-- <button type="button" class="btn btn-outline-primary btn-sm"> en vrac </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple </button> -->
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @endforeach
            @else
            <p>There are no brands</p>
            @endif


            <!-- <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-1.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>Patagonia</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Explicabo voluptatem mollitia et repellat qui dolorum quasi</p>


                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> en vrac </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>



                                                                                                                                                        </div>

                                                                                                                                                        </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-2.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>Beyond Meat.</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Aut maiores voluptates amet et quis praesentium qui senda para</p>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">


                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> produits hi-tech </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-3.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>Lush Cosmetics</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Explicabo voluptatem mollitia et repellat qui dolorum quasi</p>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> produits frais </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> en vrac </button>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div> -->
            <!-- <br> </br> -->
            <!-- <div class="row row-cols-3">

                                                                                                                                                        <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-4.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>Apple</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Explicabo quasi</p>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> en vrac </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple </button>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="400">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-5.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>Amazon</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Dolorum tempora accusamus</p>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> en vrac </button>

                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> souple </button>
                                                                                                                                                        <button type="button" class="btn btn-outline-primary btn-sm"> bois </button>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col">
                                                                                                                                                        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="400">
                                                                                                                                                        <div class="pic"><img src="{{ asset('img/companies/company-6.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>
                                                                                                                                                        <div class="member-info">
                                                                                                                                                        <h4>TOMS</h4>
                                                                                                                                                        <a href="#">
                                                                                                                                                        <span>Subcategory</span>


                                                                                                                                                        </a>
                                                                                                                                                        <p>Dolorum tcusamus</p>
                                                                                                                                                        <div class="member-footer fixed-bottom border-top">

                                                                                                                                                        <button type="button" class="btn btn-primary btn-sm"> produits culturels </button>

                                                                                                                                                        <button type="button" class="btn btn-primary btn-sm"> vente en ligne </button>
                                                                                                                                                        <button type="button" class="btn btn-primary btn-sm"> bois </button>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div> -->

        </div>



    </div>
</section><!-- End Team Section -->

@endsection


@section('support-us')
<section id="cliens" class="cliens section-bg">
    <div class="section-title">
        <h2>They support us</h2>
        <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
    </div>
    <div class="container">

        <div class="row" data-aos="zoom-in">


            @if ($supports->count())
            @foreach ($supports as $support)
            <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="{{ asset('img/clients/' . $support->image) }}" class="img-fluid" alt="">
            </div>
            @endforeach
            @else
            <p>There is no supporters!</p>
            @endif

            <!-- <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-1.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-2.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-3.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-4.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-5.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div>

                                                                                                                                                        <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                                                                                                                                                        <img src="{{ asset('img/clients/client-6.png') }}" class="img-fluid" alt="">
                                                                                                                                                        </div> -->

        </div>

    </div>
</section>
<!-- End Cliens Section -->
@endsection

@section('blog')
<section id="blog" class="blog-mf sect-pt4 route">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title-box text-center">
                    <div class="section-title">
                        <h2><a href="/home/blog">Blog Posts</h2>
                        <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            @if ($blogs->count())
            @foreach ($blogs as $blog)
            <div class="col-xs-6 col-md-4">
                <div class="card card-blog">
                    <div class="card-img">
                        <a href="{{ url('home/blog', $blog) }}"><img src="{{ asset('img/blog/' . $blog->image) }}"
                                alt="" class="img-fluid"></a>
                    </div>
                    <div class="card-body">
                        <!-- <div class="card-category-box">
                                                                                                                                                        <div class="card-category">
                                                                                                                                                        <h6 class="category">ECO</h6>
                                                                                                                                                        </div>
                                                                                                                                                        </div> -->
                        <h3 class="card-title"><a href="{{ url('home/blog', $blog) }}">{{ $blog->title }}</a></h3>
                        <p class="card-description">
                            <?php echo mb_strimwidth($blog->summary, 0, 180, '...'); ?>
                        </p>
                    </div>
                    <div class="card-footer">

                        @if ($blog->tags->count())
                        @foreach ($blog->tags as $tag)
                        <div class="card-category">
                            <a href="{{ url('home/blog/tag', $tag) }}" class="category">{{ $tag->name }}</a>
                        </div>
                        @endforeach
                        @else
                        <p>There is no tags for this blog!</p>
                        @endif




                        <!-- <div class="post-date">
                                                                                                                                                        <span class="bi bi-clock"></span> 10 min
                                                                                                                                                        </div> -->
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <p>There is no blogs!</p>
            @endif

        </div>
    </div>
    </div>
</section>
@endsection



@section('footer')

<footer id="footer">

    <!-- <div class="footer-newsletter">
                                                                                                                                                        <div class="container">
                                                                                                                                                        <div class="row justify-content-center">
                                                                                                                                                        <div class="col-lg-6">
                                                                                                                                                        <h4>Join Our Newsletter</h4>
                                                                                                                                                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                                                                                                                                                        <form action="" method="post">
                                                                                                                                                        <input type="email" name="email"><input type="submit" value="Subscribe">
                                                                                                                                                        </form>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div>
                                                                                                                                                        </div> -->
    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

            <div class="row">
                <div class="col-lg-9 text-center text-lg-start">
                    <h3>Support our project</h3>
                    <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                        anim
                        id est
                        laborum.</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                    <a class="cta-btn align-middle" href="#">Support</a>
                </div>
            </div>

        </div>
    </section>
    <!-- End Cta Section -->


    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>Alternéthique.fr</h3>
                    <p>
                        A108 Adam Street <br>
                        New York, NY 535022<br>
                        United States <br><br>
                        <strong>Phone:</strong> +1 5589 55488 55<br>
                        <strong>Email:</strong> info@example.com<br>
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Methodologies</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Something</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Something</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Something</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Something</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Something</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Social Networks</h4>
                    <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Alternéthique.fr</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

            Designed by <a href="">MK</a>
        </div>
    </div>
</footer>

@endsection
