<?php

namespace App\Models;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Stores extends Model
{
    use HasFactory;

    public function brands()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);   
        return $this->belongsToMany(
            Brand::class,
            'brand_store',
            'store_id',
            'brand_id'
        );
    }

    public function categories()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);   
        return $this->belongsToMany(
            Category::class,
            'unbranded_prod',
            'stores_id',
            'category_id'


        );
    }
}