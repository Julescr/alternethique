<?php

namespace App\Models;

use App\Models\Tag;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blogs extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        // $url = route('pages.brand', $this->slug);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $this->id
            // $url
        );
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Tag::class,
            'blog_tags',
            'blog_id',
            'tag_id'
        );
    }
}