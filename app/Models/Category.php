<?php

namespace App\Models;

use App\Models\Tag;
use App\Models\Brand;
use App\Models\Stores;
use Spatie\Searchable\Searchable;
use App\Traits\SelfReferenceTrait;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model implements Searchable
{
    use HasFactory;
    use SelfReferenceTrait;

    public function getSearchResult(): SearchResult
    {
        // $url = route('pages.brand', $this->slug);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name,
            $this->id
            // $url
        );
    }

    public function blogs()
    {
        return $this->hasMany(Blogs::class);
    }

    public function brands()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Brand::class,
            'brand_category',
            'category_id',
            'brand_id'
        );
    }

    public function root_category()
    {
        $count = 0;
        $res = 0;
        foreach ($this->all_parents() as $parent1) {
            if ($count = 0) {
                $res = $parent1->id;
            }
            $count += 1;
        }
        return $res;
    }

    public function all_parents()
    {
        if ($this->parent_id == null) {
            $parents = [$this];
        } else {
            $parents = [$this];
            $parent1 = [$this->parent];
            $temp_parent = $this->parent;

            $parents = array_merge($parents, $parent1);
            while ($temp_parent->parent) {
                $temp_parent = $temp_parent->parent;
                $parent1 = [$temp_parent];
                $parents = array_merge($parents, $parent1);
            }
        }
        $res = new Collection($parents);
        return $res->reverse();
    }

    // public function ethical_brands()
    // {

    //     //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
    //     return $brands = Brand::whereHas('rank', function ($q) {
    //         $q->where('rank', '<>', 'F');
    //     })->get();
    // }

    public function tags()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Tag::class,
            'category_tags',
            'category_id',
            'tag_id'


        );
    }

    public function all_brands()
    {
        $brands = [];
        $categories = [$this];
        while (count($categories) > 0) {
            $nextCategories = [];
            foreach ($categories as $category) {
                $brands = array_merge($brands, $category->brands->where('rank', '<>', 'F')->all());
                $nextCategories = array_merge($nextCategories, $category->allChildren->all());
            }
            $categories = $nextCategories;
        }
        return new Collection($brands); //Illuminate\Database\Eloquent\Collection
    }

    public function count_brands_category()
    {

        $total = 0;



        $categories = [$this];
        while (count($categories) > 0) {
            $nextCategories = [];
            foreach ($categories as $category) {
                $total += $category->brands->where('rank', '<>', 'F')->count();
                $nextCategories = array_merge($nextCategories, $category->allChildren->all());
            }
            $categories = $nextCategories;
        }
        return $total;
    }

    public function stores()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Stores::class,
            'unbranded_prod',
            'category_id',
            'stores_id'


        );
    }
}