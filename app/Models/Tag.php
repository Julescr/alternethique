<?php

namespace App\Models;

use App\Models\Blogs;
use App\Models\Brand;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        // $url = route('pages.brand', $this->slug);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name,
            $this->id
            // $url
        );
    }

    public function blogs()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Blogs::class,
            'blog_tags',
            'tag_id',
            'blog_id'
        );
    }

    public function brands()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Brand::class,
            'brand_tags',
            'tag_id',
            'brand_id'

        );
    }

    public function categories()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Category::class,
            'category_tags',
            'tag_id',
            'category_id'

        );
    }
}