<?php

namespace App\Models;

use App\Models\Tag;
use App\Models\Stores;
use App\Models\Category;
use Spatie\Searchable\Searchable;
use App\Traits\SelfReferenceTrait;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Brand extends Model implements Searchable
{
    use HasFactory;
    use SelfReferenceTrait;

    // $brand = Brand::find(3);
    // $parent = $brand->parent; // returns parent will return a Department instance, when the current department has another department above or it will return null when there are no more departments above.
    // $children = $brand->children; //children will return a Collection containing all the departments on the next level bellow.
    // $allChildren = $brand->allChildren; // all childrens nested
    // $root = $brand->root;
    // protected $fillable = [
    //     ''
    // ]

    public function getSearchResult(): SearchResult
    {
        // $url = route('pages.brand', $this->slug);

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name,
            $this->id
            // $url
        );
    }

    public function categories()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Category::class,
            'brand_category',
            'brand_id',
            'category_id'
        );
    }

    public function alternatives()
    {
        $category_name = $this->categories->first();
        if ($category_name) {
            $result = $category_name->brands->where('rank', '<>', 'F')->where('id', '<>', $this->id)->sortBy('rank');
            // If we want to get all brands under this category
            // $result = $category_name->all_brands()->where('rank', '<>', 'F')->where('id', '<>', $this->id)->sortBy('rank');

        } else {
            $result = null;
        }
        return $result;
    }

    public function stores()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Stores::class,
            'brand_store',
            'brand_id',
            'store_id'
        );
    }

    public function tags()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Tag::class,
            'brand_tags',
            'brand_id',
            'tag_id'
        );
    }
}
