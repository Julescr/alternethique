<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class Select2SearchController extends Controller
{
    //
    public function index()
    {
    	return view('pages.search-auto');
    }

    public function selectSearch(Request $request)
    {
    	$movies = [];

        if($request->has('q')){
            $search = $request->q;
            $movies =Brand::select("id", "name")
            		->where('name', 'ilike', "%$search%")
            		->get();
            // $movies = (new Search())
            // ->registerModel(Brand::class, 'name')
            // ->search($search);
        }
        return response()->json($movies);
    }
}
