<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Blogs;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    //
    public function single_blog(Blogs $blogs)
    {

        $tags = Tag::get();
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();
        $recent_blogs = Blogs::orderBy('published_at', 'DESC')->paginate(5);
        // $blog = Blogs::where('id', $id);
        return view('pages.single-blog', [
            'blog' => $blogs,
            'tags' => $tags,
            'categories' => $parent_category,
            'recent_blogs' => $recent_blogs
        ]);
    }

    public function list_of_blogs()
    {
        $all_blogs = Blogs::orderBy('published_at', 'DESC')->get();
        $blogs = Blogs::orderBy('published_at', 'DESC')->paginate(5);
        $tags = Tag::get();
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();

        // $blog = Blogs::where('id', $id);
        return view('pages.blog', [
            'tags' => $tags,
            'recent_blogs' => $blogs,
            'all_blogs' => $all_blogs,
            'categories' => $parent_category,
            'category_name' => null,
            'tag_name' => null
        ]);
    }

    public function category_blogs(Category $category)
    {
        $category_name = $category->name;
        $all_blogs = $category->blogs;
        $blogs = Blogs::orderBy('published_at', 'DESC')->paginate(5);
        $tags = Tag::get();
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();

        // $blog = Blogs::where('id', $id);
        return view('pages.blog', [
            'tags' => $tags,
            'recent_blogs' => $blogs,
            'all_blogs' => $all_blogs,
            'categories' => $parent_category,
            'category_name' => $category_name,
            'tag_name' => null
        ]);
    }

    public function tag_blogs(Tag $tag)
    {
        $tag_name = $tag->name;
        $all_blogs_tag = $tag->blogs;
        $blogs = Blogs::orderBy('published_at', 'DESC')->paginate(5);
        $tags = Tag::get();
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();

        // $blog = Blogs::where('id', $id);
        return view('pages.blog', [
            'tags' => $tags,
            'recent_blogs' => $blogs,
            'all_blogs' => $all_blogs_tag,
            'categories' => $parent_category,
            'tag_name' => $tag_name,
            'category_name' => null
        ]);
    }
}