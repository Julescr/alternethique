<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Blogs;
use App\Models\Category;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    //
    public function list_of_categories()
    {
        $categories = Category::get()->inRandomOrder()->paginate(1);
        // $top_brands = Category::where('rank', '=', 'A')->inRandomOrder()->paginate(6);
        // $uneth_brands = Category::where('rank', '=', 'F')->orderBy('num_searched', 'DESC')->paginate(10);
        return view('pages.index', ['categories' => $categories]);
    }

    public function category_brands(Category $category)
    {
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();
        $blogs = Blogs::orderBy('published_at', 'DESC')->get();
        $tags = Tag::get();

        $category_name = $category->name;

        return view('pages.categories', [
            'category' => $category,
            'parent_categories' => $parent_category,
            'recent_blogs' => $blogs,
            'tags' => $tags,
            'category_name' => $category_name
 ]);
    }

    public function all_brands()
    {
        $parent_category = Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get();
        $blogs = Blogs::orderBy('published_at', 'DESC')->get();
        $tags = Tag::get();

        return view('pages.categories', [
            // 'category' => $category,
            'parent_categories' => $parent_category,
            'recent_blogs' => $blogs,
            'tags' => $tags,

            'category_name' => null

        ]);
    }
}