<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Blogs;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Supporters;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;



class BrandsController extends Controller
{
    public function list_of_brands()
    {
        // $brands = Brand::get();
        $top_brands = Brand::where('rank', '=', 'A')->inRandomOrder()->paginate(6);
        $uneth_brands = Brand::where('rank', '=', 'F')->orderBy('num_searched', 'DESC')->paginate(10);

        // $tags = Tag::paginate(3);

        $parent_category = Category::where('name', '=', 'Alimentation')
            ->orWhere('name', '=', 'Habillement')
            ->orWhere('name', '=', 'Cosmétique et santé')
            ->orderBy('id', 'ASC')->get();

        $supports = Supporters::orderBy('rank', 'ASC')->get();

        $blogs = Blogs::orderBy('published_at', 'DESC')->paginate(3);

        return view('pages.index', [
            'top_brands' => $top_brands,
            'uneth_brands' => $uneth_brands,
            // 'tags' => $tags,
            'parent_category' => $parent_category,
            'supports' => $supports,
            'blogs' => $blogs
        ]);
    }

    // public function search(Request $request)
    // {
    //     $search_text = $_GET['query'];
    //     $brand = Brand::where('name', 'ilike', '%' . $search_text . '%')->orderByraw('CHAR_LENGTH(name) ASC')->get();


    //     return view('pages.brand', [
    //         'brands' => $brand

    //     ]);
    // }

    public function respage(String $brand_name)
    {
        $brand = Brand::where('name', '=', $brand_name);
        return view('pages.brand', [
            'brands' => $brand

        ]);
    }



    // public function search(Request $request)
    // {
    //     $search_text = $_GET['query'];
    //     // $brand = Brand::where('name', 'ilike', '%' . $search_text . '%')->orderByraw('CHAR_LENGTH(name) ASC')->get();

    //     $searchResults = (new Search())
    //         ->registerModel(Brand::class, 'name')
    //         ->registerModel(Category::class, 'name')
    //         ->registerModel(Tag::class, 'name')
    //         ->registerModel(Blogs::class, 'title')
    //         ->search($search_text);

    //     return view('pages.test', [
    //         'searchResults' => $searchResults
    //     ]);
    // }


    public function search(Request $request)
    {
        $search_text = $_GET['query'];
        $searchResults = (new Search())
            ->registerModel(Brand::class, 'name')
            ->registerModel(Category::class, 'name')
            ->registerModel(Tag::class, 'name')
            ->registerModel(Blogs::class, 'title')
            ->search($search_text);
        $res = null;
        foreach ($searchResults->groupByType() as $type => $modelSearchResults) {
            if ($type == 'brands') {
                // $min_len = 100000;
                // foreach ($modelSearchResults as $one_res){
                //     if (strlen($one_res->title) < $min_len){
                //         $needed = $one_res;
                //     }
                // }
             $res = view('pages.brand', [
                'brands' => Brand::where('name', 'ilike', '%' . $modelSearchResults->first()->title . '%')->orderByraw('CHAR_LENGTH(name) ASC')
             ]);
             }

            if ($res == null){
                foreach ($searchResults->groupByType() as $type => $modelSearchResults) {
                if ($type == 'categories') {
                    $res = view('pages.categories', [
                        'category' =>  Category::where('name', 'ilike', '%' . $modelSearchResults->first()->title . '%')->orderByraw('CHAR_LENGTH(name) ASC')->get()->first(),
                        'category_name' => $modelSearchResults->first()->title,
                        'parent_categories' => Category::where('parent_id', '=', null)->orderBy('id', 'ASC')->get(),
                        'recent_blogs' => Blogs::orderBy('published_at', 'DESC')->get(),
                        'tags' => Tag::get()
                     ]);
                }
            }
            }
            if ($res == null){
                foreach ($searchResults->groupByType() as $type => $modelSearchResults) {
                if ($type == 'blogs') {
                    $res = view('pages.single-blog', [
                        'blog' => Blogs::where('name', 'ilike', '%' . $modelSearchResults->first()->title . '%')->orderByraw('CHAR_LENGTH(title) ASC')->get()->first()
                     ]);
                }
                }
            }

        }

        return $res;
    }


    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $destination_path = 'public/img/brands';
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            $input['image'] = $image_name;
        }
    }

    // public function people_searches_for()
    // {

    //     $uneth_brands = Brand::where('rank', '=', 'F')->inRandomOrder()->paginate(10);
    //     return view('pages.index', ['uneth_brands' => $uneth_brands]);
    // }
}
