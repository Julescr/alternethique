<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;


class TagController extends Controller
{
    //
    public function list_of_tags() {
        $tags = Tag::get()->inRandomOrder()->paginate(3);
        // $top_brands = Brand::where('rank', '=', 'A')->inRandomOrder()->paginate(6);
        // $uneth_brands = Brand::where('rank', '=', 'F')->orderBy('num_searched', 'DESC')->paginate(10);
        return view('pages.index', ['tags' => $tags]);
    }
}
